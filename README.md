# Benchmark Results
> 2019/02/06

## Overview
I wanted to run benchmarks for a few python web frameworks. The deployment
scheme up to the application server is also considered.

## Testing
`wrk` was used to create the requests.

`$ wrk -d10s http://127.0.0.1:5000`

## WSGI
### Servers
#### Bjoern
```python
import bjoern
from wsgi.*_ import app

bjoern.run(app, "127.0.0.1", 5000)
```

#### Gunicorn
`$ gunicorn -w 1 -b 127.0.0.1:5000 wsgi.*_:app`

#### Meinheld
`$ gunicorn -w 1 -b 127.0.0.1:5000 --worker-class="egg:meinheld#gunicorn_worker" wsgi.*_:app`

#### uWSGI
`$ uwsgi --http-socket 127.0.0.1:5000 -w wsgi.*_:app -L`

### Frameworks
- Bottle
- CherryPy
- Django
- Falcon
- Flask
- hug
- Pyramid

### Results
Requests per Second

| Framework | Dev Server | Bjoern   | Gunicorn | Meinheld | uWSGI    |
| --------- | ---------- | -------- | -------- | -------- | -------- |
| Bottle    | 2796.24    | 16887.25 | 3286.59  | 13086.67 | 10097.47 |
| CherryPy  | 573.79     | 2347.65  | 1505     | 2301.3   | 2166.51  |
| Falcon    | 2851.94    | 22261.21 | 3702.63  | 17288.18 | 12855.96 |
| Flask     | 716.3      | 4914     | 2217.82  | 4545.84  | 4205.7   |
| hug       | 2721.71    | 15286.33 | 3158.35  | 12056.09 | 9009.8   |
| Pyramid   | 2395.16    | 8612.61  | 2769.91  | 7790.97  | 6320.57  |

> uWSGI is giving read socket errors, but still the response.

Transfer per Second

| Framework | Dev Server | Bjoern   | Gunicorn | Meinheld | uWSGI    |
| --------- | ---------- | -------- | -------- | -------- | -------- |
| Bottle    | 475.15KB   | 1.98MB   | 577.75KB | 2.3MB    | 0.95MB   |
| CherryPy  | 89.66KB    | 412.84KB | 263.1KB  | 411.27KB | 338.52KB |
| Falcon    | 523.6KB    | 2.91MB   | 701.49KB | 3.26MB   | 1.39MB   |
| Flask     | 121.72KB   | 590.26KB | 389.88KB | 816.83KB | 406.61KB |
| hug       | 499.69KB   | 2MB      | 598.36KB | 2.28MB   | 0.97MB   |
| Pyramid   | 407KB      | 1.01MB   | 486.91KB | 1.37MB   | 611.07KB |

## ASGI
### Servers
#### Hypercorn
`$ hypercorn -w 1 -b 127.0.0.1:5000 --uvloop asgi.*_:app`

#### uvicorn
`$ uvicorn --host 127.0.0.1 --port 5000 --log-level warning asgi.*_:app`

#### Gunicorn w/ uvicorn
`$ gunicorn -w 1 -b 127.0.0.1:5000 --worker-class="uvicorn.workers.UvicornWorker" --log-level warning asgi.*_:app`

### Frameworks
    - Quart
    - Starlette

### Results
Requests per Second

| Framework | Dev Server | Hypercorn | uvicorn  | Gunicorn |
| --------- | ---------- | --------- | -------- | -------- |
| Bare      | NA         | 3716.31   | 20534.95 | 22575.83 |
| Quart     | 226.55     | 1863.27   | 3079.87  | 3091.47  |
| Starlette | 7825.99    | 3566.09   | 13034.32 | 14086.96 |

Transfer per Second

| Framework | Dev Server | Hypercorn | uvicorn  | Gunicorn |
| --------- | ---------- | --------- | -------- | -------- |
| Bare      | NA         | 635.11KB  | 3.35MB   | 3.68MB   |
| Quart     | 32.43KB    | 265.55KB  | 427.09KB | 428.7KB  |
| Starlette | 1.13MB     | 543.27KB  | 1.89MB   | 2.04MB   |

## Others
### Frameworks
#### AIOHTTP
`$ gunicorn -w 1 -b 127.0.0.1: 5000 --worker-class aiohttp.GunicornWebWorker` other.aiohttp_:app`

#### Japronto

#### Sanic
`$ python other/sanic_.py`
`$ gunicorn -w 1 -b 127.0.0.1:5000 --worker-class="sanic.worker.GunicornWorker" other.sanic_:app`

Requests per Second

| Framework | Std. Server | Gunicorn |
| --------- | ----------- | -------- |
| AIOHTTP   | 4302.02     | 5589.82  |
| Japronto  | 50568.32    | NA       |
| Sanic     | 14565.73    | 14657.27 |

Transfer per Second

| Framework | Std. Server | Gunicorn |
| --------- | ----------- | -------- |
| AIOHTTP   | 777.24KB    | 0.99MB   |
| Japronto  | 5.5MB       | NA       |
| Sanic     | 1.9MB       | 1.92MB   |
