const cluster = require('cluster')
const os = require('os')
const utils = require('./utils.js')

// Parse the arguments.
var app = utils.parseArg('app', 'fastify')
var workers = parseInt(utils.parseArg('w', os.cpus().length))

// Start an instance or fork if master.
if (cluster.isMaster) {
  for (let i = 0; i < workers; i++) {
    cluster.fork()
  }

  console.log(`Starting ${app} with ${workers} workers @ ${new Date().toISOString()}.`)

  cluster.on('exit', () => {
    process.exit(1)
  })
} else {
  require(`./${app}_`)
}
