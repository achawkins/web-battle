const Koa = require('koa')
const Router = require('koa-router')
const utils = require('./utils.js')
const app = new Koa()
const router = new Router()

router.get('/plain', async function (ctx, next) {
  ctx.body = 'Hello, world!'
  next()
})

router.get('/json', async function (ctx, next) {
  ctx.body = { message: 'Hello, World!' }
  next()
})

router.get('/sleep/:millis', async function (ctx, next) {
  var millis = parseInt(ctx.params.millis)
  await utils.sleep(millis)
  ctx.body = { message: `I slept for ${millis} milliseconds.` }
  next()
})

app.use(router.routes())
app.listen(5000)
