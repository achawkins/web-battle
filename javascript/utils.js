module.exports = {
  parseArg: (arg, def = null) => {
    var index = process.argv.findIndex(el => el === (arg.length === 1 ? '-' : '--') + arg)
    if (index === -1) {
      return def
    } else {
      return process.argv[index + 1]
    }
  },

  /* Non blocking way to do a sleep. */
  sleep: millis => {
    return new Promise(resolve => setTimeout(resolve, millis))
  }
}
