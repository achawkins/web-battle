const http = require('http')
const utils = require('./utils.js')


var srv = http.createServer(async (req, res) => {
  res.sendDate = false
  if (req.url === '/plain') {
    status = 200
    data = 'Hello, world!'
  } else if (req.url === '/json') {
    status = 200
    data = JSON.stringify({ message: 'Hello, World!' })
  } else if (req.url.slice(0, 6) === '/sleep') {
    millis = parseInt(req.url.slice(7))
    await utils.sleep(millis)
    status = 200
    data = JSON.stringify({ message: `I slept for ${millis} milliseconds.` })
  } else {
    status = 404
    data = 'Not Found'
  }

  res.writeHead(status, {
    'Content-Type': 'application/json; encoding=utf-8',
  })
  res.end(data)
})

srv.listen(5000, '127.0.0.1')
