const fastify = require('fastify')
const utils = require('./utils')
const app = module.exports = fastify({ logger: false })
const port = 5000

const plainSchema = {
  response: {
    200: {
      type: 'string'
    }
  }
}

const jsonSchema = {
  response: {
    200: {
      type: 'object',
      properties: {
        message: { type: 'string' }
      }
    }
  }
}

const sleepSchema = Object.assign({
  params: {
    type: 'object',
    properties: {
      millis: { type: 'integer' }
    }
  }
}, jsonSchema)

app.get('/plain', { schema: plainSchema }, (req, reply) => {
  reply
    .type('text/plain')
    .code(200)
    .send('Hello, world!')
})

app.get('/json', { schema: jsonSchema },  (req, reply) => {
  reply
    .type('application/json')
    .code(200)
    .send({ message: 'Hello, world!' })
})

app.get('/sleep/:millis', { schema: sleepSchema }, async (req, reply) => {
  await utils.sleep(req.params.millis)
  reply
    .type('application/json')
    .code(200)
  return { message: `I slept for ${req.params.millis} milliseconds.` }
})

app.listen(port, (err, address) => {
  if (err) throw err
  console.log(`App listenting on ${port}.`)
})
