const express = require('express')
const utils = require('./utils.js')
const app = express()
const port = 5000

app.get("/plain", async (req, res) => res.send("Hello, world!"))

app.get("/json", async (req, res) => res.send({ message: 'Hello, World!' }))

app.get("/sleep/:millis", async (req, res) => {
  var millis = parseInt(req.params.millis)
  await utils.sleep(millis)
  res.send({ message: `I slept for ${millis} milliseconds.` })
})

app.listen(port, () => console.log(`App listenting on ${port}.`))
