package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"github.com/qiangxue/fasthttp-routing"
	"github.com/valyala/fasthttp"
)

func main() {
	router := routing.New()
	router.Get("/plain", getPlain)
	router.Get("/json", getJson)
	router.Get(`/sleep/<millis:\d+>`, getSleep)

	host := "127.0.0.1:5000"
	fmt.Println("Listening on " + host)
	panic(fasthttp.ListenAndServe(host, router.HandleRequest))
}

type Message struct {
	Content string `json:"message"`
}

func getPlain(ctx *routing.Context) error {
	ctx.Write([]byte("Hello, world!"))
	return nil
}

func getJson(ctx *routing.Context) error {
	rb, _ := json.Marshal(Message{"Hello, world!"})
	ctx.SetContentType("application/json")
	ctx.Write(rb)
	return nil
}

func getSleep(ctx *routing.Context) error {
	millis, _ := strconv.Atoi(ctx.Param("millis"))
	time.Sleep(time.Duration(millis) * time.Millisecond)

	rb, _ := json.Marshal(Message{"I slept for " + strconv.Itoa(millis) + " milliseconds."})
	ctx.SetContentType("application/json")
	ctx.Write(rb)
	return nil
}
