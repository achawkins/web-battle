package main

import (
	"encoding/json"
	"fmt"
	"log"
	"regexp"
	"strconv"
	"time"

	"github.com/valyala/fasthttp"
)

func main() {
	host := "127.0.0.1:5000"
	fmt.Println("Listening on " + host)
	if err := fasthttp.ListenAndServe(host, requestHandler); err != nil {
		log.Fatalf("Error in ListenAndServe: %s", err)
	}
}

var plainPath = regexp.MustCompile(`\/plain`)
var jsonPath = regexp.MustCompile(`\/json`)
var sleepPath = regexp.MustCompile(`\/sleep\/(?P<millis>\d+)`)

func requestHandler(ctx *fasthttp.RequestCtx) {
	path := string(ctx.Path())

	switch {
	case plainPath.MatchString(path):
		getPlain(ctx)
	case jsonPath.MatchString(path):
		getJson(ctx)
	case sleepPath.MatchString(path):
		getSleep(ctx)
	default:
		ctx.Error("Unexpected path", fasthttp.StatusNotFound)
	}
}

type Message struct {
	Content string `json:"message"`
}

func getPlain(ctx *fasthttp.RequestCtx) {
	ctx.Write([]byte("Hello, world"))
}

func getJson(ctx *fasthttp.RequestCtx) {
	rb, _ := json.Marshal(Message{"Hello, world!"})
	ctx.SetContentType("application/json")
	ctx.Write(rb)
}

func getSleep(ctx *fasthttp.RequestCtx) {
	millis, _ := strconv.Atoi(sleepPath.FindStringSubmatch(string(ctx.Path()))[1])
	time.Sleep(time.Duration(millis) * time.Millisecond)

	rb, _ := json.Marshal(Message{"I slept for " + strconv.Itoa(millis) + " milliseconds."})
	ctx.SetContentType("application/json")
	ctx.Write(rb)
}
