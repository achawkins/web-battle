package main

import (
    "log"
    "net/http"
    "strconv"
    "time"

    "github.com/go-chi/chi"
    "github.com/go-chi/render"
)

type Hello struct {
    Message string `json:"message"`
}

func Routes() *chi.Mux {
    router := chi.NewRouter()
    router.Use(
        render.SetContentType(render.ContentTypeJSON),
    )

    router.Get("/plain", Plain)
    router.Get("/json", Json)
    router.Get("/sleep/{millis:[0-9]+}", Sleep)
    return router
}

func Plain(w http.ResponseWriter, r *http.Request) {
    w.Write([]byte("Hello, world!"))
}

func Json(w http.ResponseWriter, r *http.Request) {
    message := Hello{"Hello, world!"}

    render.JSON(w, r, message)
}

func Sleep(w http.ResponseWriter, r *http.Request) {
    millis, _ := strconv.Atoi(chi.URLParam(r, "millis"))

    time.Sleep(time.Duration(millis) * time.Millisecond)

    message := Hello{"I slept for " + strconv.Itoa(millis) + " milliseconds."}

    render.JSON(w, r, message)
}

func main() {
    router := Routes()

    log.Fatal(http.ListenAndServe(":5000", router))
}
