package main

import (
	"encoding/json"
	"fmt"
	"strconv"
	"time"

	"github.com/buaazp/fasthttprouter"
	"github.com/valyala/fasthttp"
)

func main() {
	router := fasthttprouter.New()
	router.GET("/plain", getPlain)
	router.GET("/json", getJson)
	router.GET(`/sleep/:millis`, getSleep)

	host := "127.0.0.1:5000"
	fmt.Println("Listening on " + host)
	panic(fasthttp.ListenAndServe(host, router.Handler))
}

type Message struct {
	Content string `json:"message"`
}

func getPlain(ctx *fasthttp.RequestCtx) {
	ctx.Write([]byte("Hello, world!"))
}

func getJson(ctx *fasthttp.RequestCtx) {
	rb, _ := json.Marshal(Message{"Hello, world!"})
	ctx.SetContentType("application/json")
	ctx.Write(rb)
}

func getSleep(ctx *fasthttp.RequestCtx) {
	millisStr, _ := ctx.UserValue("millis").(string)
	millis, _ := strconv.Atoi(millisStr)
	time.Sleep(time.Duration(millis) * time.Millisecond)

	rb, _ := json.Marshal(Message{"I slept for " + millisStr + " milliseconds."})
	ctx.SetContentType("application/json")
	ctx.Write(rb)
}
