package main

import (
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type Message struct {
	Content string `json:"message"`
}

func main() {
	http.HandleFunc("/plain", getPlain)
	http.HandleFunc("/json", getJSON)
	http.HandleFunc("/sleep/", getSleep)
	http.ListenAndServe(":5000", nil)
}

func getPlain(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello, world!"))
}

func getJSON(w http.ResponseWriter, r *http.Request) {
	message := Message{"Hello, World!"}

	js, err := json.Marshal(message)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func getSleep(w http.ResponseWriter, r *http.Request) {
	millis, err := strconv.Atoi(strings.Replace(r.URL.Path, "/sleep/", "", 1))

	time.Sleep(time.Duration(millis) * time.Millisecond)

	message := Message{"I slept for " + strconv.Itoa(millis) + " milliseconds."}

	js, err := json.Marshal(message)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}
