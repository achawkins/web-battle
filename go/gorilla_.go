package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

type Hello struct {
	Message string `json:"message"`
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/plain", Plain)
	r.HandleFunc("/json", GetJson)
	r.HandleFunc("/sleep/{millis:[0-9]+}", GetSleep)
	log.Fatal(http.ListenAndServe(":5000", r))
}

func Plain(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello, world!"))
}

func GetJson(w http.ResponseWriter, r *http.Request) {
	message := Hello{"Hello, world!"}

	js, err := json.Marshal(message)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func GetSleep(w http.ResponseWriter, r *http.Request) {
	millis, _ := strconv.Atoi(mux.Vars(r)["millis"])

	time.Sleep(time.Duration(millis) * time.Millisecond)

	message := Hello{"I slept for " + strconv.Itoa(millis) + " milliseconds."}

	js, err := json.Marshal(message)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}
