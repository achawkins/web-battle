package main

import (
	"net/http"
	"strconv"
	"time"

	"github.com/go-ozzo/ozzo-routing"
	"github.com/go-ozzo/ozzo-routing/content"
)

type Hello struct {
	Message string `json:"message"`
}

func main() {
	router := routing.New()
	router.Use(
		content.TypeNegotiator(content.JSON),
	)
	router.Get("/plain", getPlain)
	router.Get("/json", getJson)
	router.Get(`/sleep/<millis:\d+>`, getSleep)

	http.Handle("/", router)
	http.ListenAndServe(":5000", nil)
}

func getPlain(c *routing.Context) error {
	return c.Write([]byte("Hello, world!"))
}

func getJson(c *routing.Context) error {
	message := Hello{"Hello, world!"}
	return c.Write(message)
}

func getSleep(c *routing.Context) error {
	millis, _ := strconv.Atoi(c.Param("millis"))
	time.Sleep(time.Duration(millis) * time.Millisecond)
	message := Hello{"I slept for " + strconv.Itoa(millis) + " milliseconds."}
	return c.Write(message)
}
