import asyncio
from ujson import dumps


class app():
    def __init__(self, scope):
        assert scope["type"] == "http"
        self.scope = scope

    async def __call__(self, receive, send):
        await send({
            "type": "http.response.start",
            "status": 200,
            "headers": [
                [b"content-type", b"application/json"]
            ],
        })
        if self.scope["path"] == "/json":
            await send({
                "type": "http.response.body",
                "body": dumps({"message": "Hello, World!"}).encode()
            })
        elif self.scope["path"][:6] == "/sleep":
            millis = int(self.scope["path"][7:])
            await asyncio.sleep(millis / 1000)
            await send({
                "type": "http.response.body",
                "body": dumps({
                    "message": "I slept for {} milliseconds.".format(millis),
                }).encode()
            })
        else:
            await send({
                "type": "http.response.body",
                "body": b"404 - Not Found"
            })
