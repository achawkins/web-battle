import hug
import time
from wsgiref import simple_server


@hug.get("/json")
def get_json():
    return {"message": "Hello, World!"}


@hug.get("/sleep/{millis}")
def get_sleep(millis):
    time.sleep(int(millis) / 1000)
    return {"message": "I slept for {} milliseconds.".format(millis)}


app = __hug_wsgi__


def serve_dev(host, port):
    httpd = simple_server.make_server(host, port, app)
    httpd.serve_forever()


if __name__ == "__main__":
    serve_dev("127.0.0.1", 5000)
