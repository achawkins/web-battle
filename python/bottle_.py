from bottle import Bottle, run
import time


app = Bottle()


@app.route("/json")
def get_json():
    return {"message": "Hello, World!"}


@app.route("/sleep/<millis:int>")
def get_sleep(millis):
    time.sleep(millis / 1000)
    return {"message": "I slept for {} milliseconds.".format(millis)}


def serve_dev(host, port):
    run(app, host=host, port=port)


if __name__ == "__main__":
    serve_dev("127.0.0.1", 5000)
