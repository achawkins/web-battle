from pyramid.config import Configurator
from pyramid.view import view_config
import time
from wsgiref import simple_server


@view_config(renderer="json")
def get_json(request):
    return {"message": "Hello, World!"}


@view_config(renderer="json")
def get_sleep(request):
    millis = request.matchdict["millis"]
    time.sleep(int(millis) / 1000)
    return {"message": "I slept for {} milliseconds.".format(millis)}


with Configurator() as config:
    config.add_route("json", "/json")
    config.add_route("sleep", "/sleep/{millis}")
    config.add_view(get_json, route_name="json", renderer="json")
    config.add_view(get_sleep, route_name="sleep", renderer="json")
    app = config.make_wsgi_app()


def serve_dev(host, port):
    httpd = simple_server.make_server(host, port, app)
    httpd.serve_forever()


if __name__ == "__main__":
    serve_dev("127.0.0.1", 5000)
