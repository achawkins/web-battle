from flask import Flask, jsonify
import time


app = Flask(__name__)


@app.route("/json")
def get_json():
    return jsonify({"message": "Hello, World!"})


@app.route("/sleep/<int:millis>")
def get_sleep(millis):
    time.sleep(millis / 1000)
    return jsonify({"message": "I slept for {} milliseconds.".format(millis)})


def serve_dev(host, port):
    app.run(host=host, port=port)


if __name__ == "__main__":
    serve_dev("127.0.0.1", 5000)
