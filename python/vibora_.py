import asyncio
from vibora import Vibora
from vibora.responses import JsonResponse, Response


app = Vibora()


@app.route("/plain")
async def plain():
    return Response(b"Hello, world!")


@app.route("/json")
async def json():
    return JsonResponse({"message": "Hello, world!"})


@app.route("/sleep/<millis>")
async def sleep(millis: int):
    await asyncio.sleep(millis / 1000)
    return JsonResponse({"message": f"I slept for {millis} millisesconds."})


if __name__ == "__main__":
    app.run(host="127.0.0.1", port=5000, debug=False, necromancer=True)
