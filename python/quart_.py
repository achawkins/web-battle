from quart import Quart, jsonify
import asyncio


app = Quart(__name__)


@app.route("/json")
async def get_json():
    return jsonify({"hello": "world"})


@app.route("/sleep/<int:millis>")
async def get_sleep(millis):
    await asyncio.sleep(millis / 1000)
    return jsonify({"message": "I slept for {} milliseconds.".format(millis)})


def serve_dev(host, port):
    app.run(host=host, port=port)


if __name__ == "__main__":
    serve_dev("127.0.0.1", 5000)
