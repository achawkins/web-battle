import asyncio
import responder


api = responder.API()


@api.route("/plain")
async def get_plain(req, resp):
    resp.text = "Hello world!"


@api.route("/json")
async def get_json(req, resp):
    resp.media = {"message": "Hello world!"}


@api.route("/sleep/{millis}")
async def get_sleep(req, resp, *, millis):
    await asyncio.sleep(int(millis) / 1000)
    resp.media = {"message": f"I slept for {millis} milliseconds."}
