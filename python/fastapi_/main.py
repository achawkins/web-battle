import asyncio
from fastapi import FastAPI
from starlette.responses import UJSONResponse, PlainTextResponse


app = FastAPI()


@app.get("/plain")
async def get_plain():
    return PlainTextResponse("Hello world!")


@app.get("/json")
async def get_json():
    return UJSONResponse({"message": "Hello world!"})


@app.get("/sleep/{millis}")
async def get_sleep(millis: int):
    await asyncio.sleep(millis / 1000)
    return UJSONResponse({"message": f"I slept for {millis} milliseconds."})
