from aiohttp import web
import asyncio


async def get_plain(request):
    return web.Response(body="Hello, world!")


async def get_json(request):
    return web.json_response({"message": "Hello, World!"})


async def get_sleep(request):
    millis = int(request.match_info["millis"])
    await asyncio.sleep(millis / 1000)
    return web.json_response({
        "message": "I slept for {} milliseconds".format(millis),
    })


app = web.Application()
app.add_routes([
    web.get("/plain", get_plain),
    web.get("/json", get_json),
    web.get("/sleep/{millis}", get_sleep),
])


def serve_dev(host, port):
    web.run_app(app, host=host, port=port)


if __name__ == "__main__":
    serve_dev("127.0.0.1", 5000)
