import asyncio
from blacksheep.server import Application
from blacksheep import Response, TextContent, JsonContent
from blacksheep.options import ServerOptions
import multiprocessing


app = Application(options=ServerOptions(
    host="127.0.0.1",
    port=5000,
    processes_count=multiprocessing.cpu_count(),
))


@app.route("/plain")
async def get_plain(request):
    return Response(200, content=TextContent("Hello world!"))


@app.route("/json")
async def get_json(request):
    return Response(200, content=JsonContent({"message": "Hello world!"}))


@app.route("/sleep/:millis")
async def get_sleep(request, millis: int):
    await asyncio.sleep(millis / 1000)
    return Response(200, content=JsonContent({
        "message": f"I slept for {millis} milliseconds.",
    }))

app.start()
