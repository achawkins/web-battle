import cherrypy
import time


class HelloWorld:
    @cherrypy.expose
    @cherrypy.tools.json_out()
    def json(self):
        return {"message": "Hello, World"}

    @cherrypy.expose
    @cherrypy.popargs("millis")
    @cherrypy.tools.json_out()
    def sleep(self, millis):
        time.sleep(int(millis) / 1000)
        return {"message": "I slept for {} milliseconds.".format(millis)}


access_log = cherrypy.log.access_log
for handler in tuple(access_log.handlers):
    access_log.removeHandler(handler)
app = cherrypy.Application(HelloWorld())


def serve_dev(host, port):
    cherrypy.config.update({
        "server.socket_host": host,
        "server.socket_port": port,
    })
    cherrypy.quickstart(HelloWorld())


if __name__ == "__main__":
    serve_dev("127.0.0.1", 5000)
