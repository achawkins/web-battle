import argparse
import multiprocessing
import subprocess

DEPLOYMENTS = {
    "wsgi": ["bjoern", "gunicorn", "meinheld", "uwsgi"],
    "asgi": ["hypercorn", "uvicorn"],
}

FRAMEWORKS = {
    "bottle": ["dev", "wsgi"],
    "cherrypy": ["dev", "wsgi"],
    "falcon": ["dev", "wsgi"],
    "flask": ["dev", "wsgi"],
    "hug": ["dev", "wsgi"],
    "pyramid": ["dev", "wsgi"],
    "quart": ["dev", "asgi"],
    "starlette": ["dev", "asgi"],
    "aiohttp": ["dev", "aiohttp"],
    "japronto": ["dev", "prod"],
    "sanic": ["dev", "prod", "sanic"],
}

GUNICORN_WORKERS = {
    "gunicorn": "sync",
    "meinheld": "egg:meinheld#gunicorn_worker",
    "uvicorn": "uvicorn.workers.UvicornWorker",
    "aiohttp": "aiohttp.GunicornWebWorker",
    "sanic": "sanic.worker.GunicornWorker",
}

GUNICORN_COMMAND = (
    "{command} -w {workers} -b {bind} --log-level warning "
    "--worker-class {worker} {framework}_:app"
)
UWSGI_COMMAND = (
    "uwsgi --http-socket {bind} -L -p {workers} "
    "-w {framework}_:app"
)

# Get all unique deployments.
ALL_DEPLOYMENTS = []
for scheme in FRAMEWORKS.values():
    for deploy in scheme:
        if deploy in DEPLOYMENTS:
            ALL_DEPLOYMENTS.extend(DEPLOYMENTS[deploy])
        else:
            ALL_DEPLOYMENTS.append(deploy)
ALL_DEPLOYMENTS = set(ALL_DEPLOYMENTS)

# Create the parser.
parser = argparse.ArgumentParser()
parser.add_argument(
    "--framework",
    "-f",
    default="flask",
    choices=FRAMEWORKS.keys(),
)
parser.add_argument(
    "--deployment",
    "-d",
    default="uwsgi",
    choices=ALL_DEPLOYMENTS,
)
parser.add_argument("--workers", "-w", default=multiprocessing.cpu_count())
parser.add_argument("--bind", "-b", default="127.0.0.1:5000")

# Parse the arguments.
args = parser.parse_args()

# Make sure all frameworks match deployments.
valid = False
for scheme in FRAMEWORKS[args.framework]:
    if scheme in DEPLOYMENTS:
        for deploy in DEPLOYMENTS[scheme]:
            if deploy == args.deployment:
                valid = True
    else:
        if scheme == args.deployment:
            valid = True
if not valid:
    parser.error("{} deployment is not valid for {} framework".format(
        args.deployment,
        args.framework,
    ))

# Split the host and port.
HOST, port = args.bind.split(':')
PORT = int(port)

# Handle each of the deployment methods.
if args.deployment == "dev":
    exec("from {}_ import serve_dev".format(args.framework), globals())
    serve_dev(HOST, PORT)
elif args.deployment == "prod":
    exec("from {}_ import serve_prod".format(args.framework), globals())
    serve_prod(HOST, PORT, args.workers)
elif args.deployment == "bjoern":
    import bjoern
    exec("from {}_ import app".format(args.framework), globals())
    bjoern.run(app, HOST, PORT)
elif args.deployment in GUNICORN_WORKERS:
    proc = subprocess.Popen(GUNICORN_COMMAND.format(**{
        "command": "gunicorn",
        "workers": args.workers,
        "bind": args.bind,
        "worker": GUNICORN_WORKERS[args.deployment],
        "framework": args.framework,
    }).split())
    proc.wait()
elif args.deployment == "hypercorn":
    proc = subprocess.Popen(GUNICORN_COMMAND.format(**{
        "command": "hypercorn",
        "workers": args.workers,
        "bind": args.bind,
        "worker": "uvloop",
        "framework": args.framework,
    }).split())
    proc.wait()
elif args.deployment == "uwsgi":
    proc = subprocess.Popen(UWSGI_COMMAND.format(**{
        "bind": args.bind,
        "workers": args.workers,
        "framework": args.framework,
    }).split())
    proc.wait()
