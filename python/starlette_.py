from starlette.applications import Starlette
from starlette.responses import Response, UJSONResponse
import uvicorn
import asyncio


app = Starlette(debug=False)


@app.route("/plain")
async def get_plain(request):
    return Response("Hello, world!")


@app.route("/json")
async def get_json(request):
    return UJSONResponse({"message": "Hello, World!"})


@app.route("/sleep/{millis}")
async def get_sleep(request):
    millis = int(request.path_params["millis"])
    await asyncio.sleep(millis / 1000)
    return UJSONResponse({
        "message": f"I slept for {millis} milliseconds.",
    })


def serve_dev(host, port):
    uvicorn.run(app, host=host, port=port)


if __name__ == "__main__":
    serve_dev("127.0.0.1", 5000)
