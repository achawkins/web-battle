from japronto import Application
import asyncio


async def get_json(request):
    return request.Response(json={"message": "Hello, World!"})


async def get_sleep(request):
    millis = int(request.match_dict["millis"])
    await asyncio.sleep(millis / 1000)
    return request.Response(json={
        "message": "I slept for {} milliseconds.".format(millis),
    })


app = Application()

app.router.add_route("/json", get_json, method="GET")
app.router.add_route("/sleep/{millis}", get_sleep, method="GET")


def serve_dev(host, port):
    app.run(host=host, port=port, worker_num=1)


def serve_prod(host, port, workers):
    app.run(host=host, port=port, worker_num=workers)


if __name__ == "__main__":
    serve_dev("127.0.0.1", 5000)
