import asyncio
from bocadillo import App


app = App()


@app.route("/plain")
async def get_plain(req, res):
    res.text = "Hello world!"


@app.route("/json")
async def get_json(req, res):
    res.json = {"message": "Hello world!"}


@app.route("/sleep/{millis}")
async def get_sleep(req, res, millis: int):
    await asyncio.sleep(millis / 1000)
    res.json = {"message": f"I slept for {millis} milliseconds."}
