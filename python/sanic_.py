from sanic import Sanic, response
import asyncio


app = Sanic(__name__)


@app.route("/plain", methods=["GET"])
async def get_plain(request):
    return response.text("Hello, world!")


@app.route("/json", methods=["GET"])
async def get_json(request):
    return response.json({"message": "Hello, World!"})


@app.route("/sleep/<millis:int>", methods=["GET"])
async def get_sleep(request, millis):
    await asyncio.sleep(millis / 1000)
    return response.json({
        "message": f"I slept for {millis} milliseconds.",
    })


def serve_dev(host, port):
    app.run(host=host, port=port)


def serve_prod(host, port, workers):
    app.run(
        host=host,
        port=port,
        debug=False,
        access_log=False,
        workers=workers,
    )


if __name__ == "__main__":
    serve_dev("127.0.0.1", 5000)
