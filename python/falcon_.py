import falcon
from wsgiref import simple_server
import time


class JsonRoute:
    def on_get(self, req, resp):
        resp.media = {"message": "Hello, World!"}


class SleepRoute:
    def on_get(self, req, resp, millis):
        time.sleep(millis / 1000)
        resp.media = {"message": "I slept for {} milliseconds.".format(millis)}


app = falcon.API()
app.add_route("/json", JsonRoute())
app.add_route("/sleep/{millis:int}", SleepRoute())


def serve_dev(host, port):
    httpd = simple_server.make_server(host, port, app)
    httpd.serve_forever()


if __name__ == "__main__":
    serve_dev("127.0.0.1", 5000)
