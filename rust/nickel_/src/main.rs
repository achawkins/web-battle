#[macro_use] extern crate nickel;
extern crate serde;
extern crate serde_json;
#[macro_use] extern crate serde_derive;

use std::{thread, time};
use nickel::status::StatusCode;
use nickel::{Nickel, HttpRouter};

#[derive(Serialize)]
struct Hello {
    message: String,
}

fn main() {
    let mut server = Nickel::new();

    server.get("/json", middleware! { |_req|
        let hello = Hello{message: "Hello, world!".to_string()};
        serde_json::to_value(hello).map_err(|e| (StatusCode::InternalServerError, e))
    });

    server.get("/sleep/:millis", middleware! { |req|
        let millis = req.param("millis").unwrap().parse::<u64>().unwrap();
        let milli_dur = time::Duration::from_millis(millis);
        let now = time::Instant::now();
        thread::sleep(milli_dur);
        assert!(now.elapsed() >= milli_dur);
        serde_json::to_value(Hello{message: format!("I slept for {} milliseconds.", millis)}).map_err(|e| (StatusCode::InternalServerError, e))
    });

    server.listen("127.0.0.1:5000").unwrap();
}
