#[macro_use]
extern crate tower_web;
extern crate tokio;

#[macro_use]
extern crate serde_json;

use tower_web::ServiceBuilder;

use std::{thread, time};

#[derive(Clone, Debug)]
struct JsonResource;

#[derive(Debug, Response)]
struct Message {
    message: String,
}

impl_web! {
    impl JsonResource {
        #[get("/json")]
        #[content_type("json")]
        fn get_json(&self) -> Result<Message, ()> {
            Ok(Message{message: "Hello, world!".to_owned()})
        }

        #[get("/sleep/:millis")]
        #[content_type("json")]
        fn get_sleep(&self, millis: u64) -> Result<Message, ()> {
            let milli_dur = time::Duration::from_millis(millis);
            let now = time::Instant::now();
            thread::sleep(milli_dur);
            assert!(now.elapsed() >= milli_dur);
            Ok(Message{message: format!("I slept for {} milliseconds.", millis)})
        }
    }
}

fn main() {
    let addr = "127.0.0.1:5000".parse().expect("Invalid address");
    println!("Listening on http://{}", addr);

    ServiceBuilder::new()
        .resource(JsonResource)
        .run(&addr)
        .unwrap();
}
