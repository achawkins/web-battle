extern crate thruster;
extern crate futures;

use std::boxed::Box;
use std::{thread, time};
use futures::future;

use thruster::{App, BasicContext as Ctx, MiddlewareChain, MiddlewareReturnValue, Request, middleware};
use thruster::builtins::server::Server;
use thruster::server::ThrusterServer;

fn plaintext(mut context: Ctx, _next: impl Fn(Ctx) -> MiddlewareReturnValue<Ctx> + Send + Sync) -> MiddlewareReturnValue<Ctx> {
    let val = "Hello, world!";
    context.body(val);
    context.content_type("text/plain");

    Box::new(future::ok(context))
}

fn json(mut context: Ctx, _next: impl Fn(Ctx) -> MiddlewareReturnValue<Ctx> + Send + Sync) -> MiddlewareReturnValue<Ctx> {
    let val = r#"{"message":"Hello, world!"}"#;
    context.body(val);
    context.content_type("application/json");

    Box::new(future::ok(context))
}

fn sleep(mut context: Ctx, _next: impl Fn(Ctx) -> MiddlewareReturnValue<Ctx> + Send + Sync) -> MiddlewareReturnValue<Ctx> {
    // let millis = req.match_info().query("millis").unwrap();
    let milli_dur = time::Duration::from_millis(50);
    let now = time::Instant::now();
    thread::sleep(milli_dur);
    assert!(now.elapsed() >= milli_dur);

    let val = r#"{"message":"I slept for 50 milliseconds."}"#;
    context.body(val);
    context.content_type("application/json");

    Box::new(future::ok(context))
}


fn main() {
    let host = "127.0.0.1";
    let port = 5000;
    println!("Starting server...");

    let mut app = App::<Request, Ctx>::new_basic();

    app.get("/plaintext", middleware![Ctx => plaintext]);
    app.get("/json", middleware![Ctx => json]);
    app.get("/sleep/*", middleware![Ctx => sleep]);

    let server = Server::new(app);
    println!("Running Thruster on {}:{}", host, port);
    server.start(host, port);
}
