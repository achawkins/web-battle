extern crate gotham;
#[macro_use] extern crate gotham_derive;
extern crate hyper;
extern crate mime;
extern crate serde;
#[macro_use] extern crate serde_derive;
extern crate serde_json;

use hyper::{Body, Response, StatusCode};

use gotham::handler::IntoResponse;
use gotham::helpers::http::response::create_response;
use gotham::router::builder::*;
use gotham::state::{FromState, State};
use gotham::router::Router;
use std::{thread, time};

#[derive(Deserialize, StateData, StaticResponseExtender)]
struct PathExtractor {
    millis: u64,
}

#[derive(Serialize)]
struct Hello {
    message: String,
}

impl IntoResponse for Hello {
    fn into_response(self, state: &State) -> Response<Body> {
        create_response(
            state,
            StatusCode::OK,
            mime::APPLICATION_JSON,
            serde_json::to_string(&self).expect("serialized hello"),
        )
    }
}

fn json(state: State) -> (State, Hello) {
    (state, Hello{message: "Hello, world!".to_string()})
}

fn sleep(state: State) -> (State, Hello) {
    let path = PathExtractor::borrow_from(&state);
    let milli_dur = time::Duration::from_millis(path.millis);
    let now = time::Instant::now();
    thread::sleep(milli_dur);
    assert!(now.elapsed() >= milli_dur);
    let msg = Hello{message: format!("I slept for {} milliseconds.", path.millis)};
    (state, msg)
}

fn router() -> Router {
    build_simple_router(|route| {
        route.get("/json").to(json);
        route.get("/sleep/:millis").with_path_extractor::<PathExtractor>().to(sleep);
    })
}

fn main() {
    gotham::start("127.0.0.1:5000", router());
}
