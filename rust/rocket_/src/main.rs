#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate serde_derive;

use std::{thread, time};
use rocket_contrib::json::Json;

#[derive(Serialize)]
struct Hello {
    message: String,
}

#[get("/json")]
fn json() -> Json<Hello> {
    Json(Hello{message: "Hello, world!".to_string()})
}

#[get("/sleep/<millis>")]
fn sleep(millis: u64) -> Json<Hello> {
    let milli_dur = time::Duration::from_millis(millis);
    let now = time::Instant::now();
    thread::sleep(milli_dur);
    assert!(now.elapsed() >= milli_dur);
    Json(Hello{message: format!("I slept for {} milliseconds.", millis)})
}

fn main() {
    rocket::ignite().mount("/", routes![json, sleep]).launch();
}
