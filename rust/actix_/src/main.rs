#[macro_use]
extern crate log;
#[macro_use]
extern crate serde_derive;

use std::{io, time};

use actix_web::{web, App, Error, HttpServer, HttpResponse};
use futures::future::{result, Future};
use tokio_timer::sleep;

#[derive(Serialize)]
struct Hello {
    message: String,
}

fn get_plain() -> HttpResponse {
    HttpResponse::Ok()
        .body("Hello, world!")
}

fn get_json() -> HttpResponse {
    let message = Hello{
        message: "Hello, world!".to_owned()
    };
    let json = serde_json::to_string(&message).unwrap();
    HttpResponse::Ok()
        .content_type("application/json")
        .body(json)
}

#[derive(Deserialize)]
struct SleepParams {
    millis: u64,
}

fn get_sleep(params: web::Path<SleepParams>) -> impl Future<Item = HttpResponse, Error = Error> {
    let millis = time::Duration::from_millis(params.millis);
    sleep(millis)
        .then(move |_| {
            let message = Hello{
                message: format!("I slept for {} milliseconds.", params.millis)
            };
            let json = serde_json::to_string(&message).unwrap();
            result(Ok(HttpResponse::Ok()
                .content_type("application/json")
                .body(json)))
        })
}

fn main() -> io::Result<()> {
    let app = move || {
        debug!("Constructing the App");
        App::new()
            .service(web::resource("/plain").route(web::get().to(get_plain)))
            .service(web::resource("/json").route(web::get().to(get_json)))
            .service(web::resource("/sleep/{millis}").route(web::get().to_async(get_sleep)))
    };

    debug!("Starting server");
    HttpServer::new(app).bind("127.0.0.1:5000")?.run()
}
