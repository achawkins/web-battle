#![deny(warnings)]
extern crate tokio;
extern crate warp;

use std::str::FromStr;
use std::time::{Instant, Duration};
use tokio::timer::Delay;
use warp::{Filter, Future};

struct Millis(u64);

impl FromStr for Millis {
    type Err = ();
    fn from_str(src: &str) -> Result<Self, Self::Err> {
        src.parse::<u64>().map_err(|_| ()).and_then(|num| {
            if num <= 1000 {
                Ok(Millis(num))
            } else {
                Err(())
            }
        })
    }
}

fn main() {
    pretty_env_logger::init();

    let get_json = warp::path("json").map(|| "Hello, world!".to_owned());

    let sleep = warp::path::param()
        .and_then(|Millis(millis)| {
            Delay::new(Instant::now() + Duration::from_millis(millis))
                .map(move |()| millis)
                .map_err(|timer_err| {
                    eprintln!("timer error: {}", timer_err);
                    warp::reject::custom(timer_err)
                })
        })
        .map(|millis| format!("I waited {} milliseconds.", millis));
    let get_sleep = warp::path("sleep").and(sleep);

    let routes = warp::get2().and(get_json.or(get_sleep));

    warp::serve(routes).run(([127, 0, 0, 1], 5000))
}
