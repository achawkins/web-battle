#![deny(warnings)]
extern crate futures;
extern crate hyper;
extern crate pretty_env_logger;
extern crate serde;
extern crate serde_json;

use std::{thread, time};

use futures::{future, Future};

use hyper::{Body, Method, Request, Response, Server, StatusCode, header};
use hyper::service::service_fn;

use serde::Serialize;

type GenericError = Box<dyn std::error::Error + Send + Sync>;
type ResponseFuture = Box<dyn Future<Item=Response<Body>, Error=GenericError> + Send>;

#[derive(Serialize)]
struct Message {
    content: String,
}

fn get_json() -> ResponseFuture {
    let data = Message{content: "Hello, world!".to_owned()};
    let res = match serde_json::to_string(&data) {
        Ok(json) => {
            Response::builder()
                .header(header::CONTENT_TYPE, "application/json")
                .body(Body::from(json))
                .unwrap()
        },
        Err(_) => {
            Response::builder()
                .status(StatusCode::INTERNAL_SERVER_ERROR)
                .body(Body::from("Internal Server Error"))
                .unwrap()
        }
    };

    Box::new(future::ok(res))
}

fn get_sleep(req: Request<Body>) -> ResponseFuture {
    let path: Vec<&str> = req.uri().path().split("/").collect();
    let millis_str = path[2].to_owned();
    let millis = millis_str.parse::<u64>().unwrap();
    let milli_dur = time::Duration::from_millis(millis);
    thread::sleep(milli_dur);
    let data = Message{content: format!("I slept for {} milliseconds", millis)};
    let res = match serde_json::to_string(&data) {
        Ok(json) => {
            Response::builder()
                .status(StatusCode::OK)
                .header(header::CONTENT_TYPE, "application/json")
                .body(Body::from(json))
                .unwrap()
        },
        Err(_) => {
            Response::builder()
                .status(StatusCode::INTERNAL_SERVER_ERROR)
                .body(Body::from("Internal Server Error"))
                .unwrap()
        }
    };

    Box::new(future::ok(res))
}

fn router(req: Request<Body>) -> ResponseFuture {
    if req.method() == &Method::GET && req.uri().path().starts_with("/sleep") {
        get_sleep(req)
    } else {
        match (req.method(), req.uri().path()) {
            (&Method::GET, "/json") => {
                get_json()
            },
            _ => {
                let body = Body::from("404 - Not Found");
                Box::new(future::ok(Response::builder()
                    .status(StatusCode::NOT_FOUND)
                    .body(body)
                    .unwrap()))
            }
        }
    }
}

fn main() {
    pretty_env_logger::init();

    let addr = ([127, 0, 0, 1], 5000).into();

    hyper::rt::run(future::lazy(move || {
        let new_service = || {
            service_fn(|req| {
                router(req)
            })
        };

        let server = Server::bind(&addr)
            .serve(new_service)
            .map_err(|e| eprintln!("server error: {}", e));

        println!("Listening on http://{}", addr);

        server
    }));
}
