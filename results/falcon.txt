Falcon
======

12 Workers - Meinheld - 100 Connections
Running 10s test @ http://127.0.0.1:5000
  2 threads and 100 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency     1.17ms    1.85ms  28.37ms   94.83%
    Req/Sec    57.44k     7.90k   73.13k    61.00%
  1142867 requests in 10.01s, 215.80MB read
Requests/sec: 114222.86
Transfer/sec:     21.57MB
